import React from 'react';
import {Button, StyleSheet, Text, View, Alert, ScrollView} from 'react-native';
import Bananas from "./components/Bananas";
import Blink from "./components/Blink";
import LottieView from 'lottie-react-native';

class Greeting extends React.Component {
  render() {
    return (
      <Text>Greetings {this.props.name}!</Text>
    )
  }
}

export default class App extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.red}>Hello World!</Text>
          <Greeting name='Darush'/>
          <Bananas/>
          <Blink text='I am blinking...'/>
          <Button onPress={() => {
            Alert.alert("Title", "Message description.");
          }}
                  title="Click me!"
          />
          <LottieView
            source={require('./raw/pink_judge_perfect.json')}
            autoPlay
            loop
            style = {styles.lottie}
            imageAsse
          />
        </View>
        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fffaaa',
    alignItems: 'center',
    justifyContent: 'center',
  },
  red: {
    color: 'red',
    backgroundColor: 'powderblue',
    width: 120,
    height: 120,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  lottie: {
    width:50,
    height: 50,
  }
});
