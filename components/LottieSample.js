import React, {Component} from 'react'
import LottieView from 'lottie-react-native';

export default class LottieSample extends Component {
  componentDidMount() {
    this.animation.play();
  }
  
  render() {
    return (
      <LottieView
        ref={animation => {
          this.animation = animation;
        }}
        source={require('raw/pink_judge_perfect.json')}
      />
    )
  }
  
}