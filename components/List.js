import React, {Component} from 'react'
import {FlatList} from 'react-native'

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        {id:'100', title: 'Devin'},
        {id:'101', title: 'Jackson'},
        {id:'102', title: 'James'},
        {id:'103', title: 'Joel'},
        {id:'104', title: 'John'},
        {id:'105', title: 'Jillian'},
        {id:'106', title: 'Jimmy'},
        {id:'107', title: 'Julie'},
      ]
    };
  }
  
  componentDidMount() {
    return fetch('https://facebook.github.io/react-native/movies.json')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(prevState => ({
          dataSource: this.state.dataSource.concat(responseJson.movies)
        }), function () {

        });
        
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  render() {
    return (
      <FlatList
        data={this.state.dataSource}
        renderItem={({item}) => <Text style={styles.item}>{item.title}</Text>}
        keyExtractor={({id}, index) => id}
      />
    );
  }
}

function getMoviesFromApiAsync() {
  return fetch('https://facebook.github.io/react-native/movies.json')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson.movies;
    })
    .catch((error) => {
      console.error(error);
    });
}


const styles = StyleSheet.create({
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
